module Lib
    ( someFunc
    ) where

{-import Data.Word-}
{-import Foreign.Ptr-}
{-import Foreign.StablePtr-}

{-arrSize :: Int-}
{-arrSize = 10000000-}

someFunc :: IO ()
someFunc = do
    return ()
    {-arr <- newByteArray (arrSize * 8)-}
    {-compactedArr <- getCompact <$> compact arr-}
    {-forM_ [0..arrSize - 1] $ \i -> do-}
    {-    val <- compact (i, "World")-}
    {-    let (IntPtr ptr) = ptrToIntPtr $ castStablePtrToPtr stable-}
    {-        writePtr     = fromIntegral ptr :: Int64-}
    {-    writeByteArray compactedArr i writePtr-}

    {-forM_ [0..arrSize - 1] $ \i -> do-}
    {-    value <- readByteArray compactedArr i :: IO Int64-}
    {-    writeByteArray compactedArr i (value + 1)-}

    {-tenth <- readByteArray compactedArr 10 :: IO Int64-}
    {-eleventh <- readByteArray compactedArr 11 :: IO Int64-}
    {-print tenth-}
    {-print eleventh-}
