{-# LANGUAGE MagicHash #-}

module HashTable where

import Control.Monad.Primitive
import Control.Monad.ST
import Data.Word (Word8)
import Data.Hashable
import Data.Primitive.ByteArray
import GHC.Exts

data HashTable s k v = HashTable
    { _load      :: !(MutableByteArray s)
    , _keyStates :: !(MutableByteArray s)
    , _keys      :: !(SmallMutableArray# s k)
    , _values    :: !(SmallMutableArray# s v)
    , _size      :: !(MutableByteArray s)
    }

newHashTable :: Int -> ST s (HashTable s k v)
newHashTable = undefined

readSmallArray :: SmallMutableArray# s a -> Int -> ST s a
readSmallArray arr (I# i#) = primitive (readSmallArray# arr i#)
{-# INLINE readSmallArray #-}

writeSmallArray :: SmallMutableArray# s a -> Int -> a -> ST s ()
writeSmallArray arr (I# i#) x = primitive_ (writeSmallArray# arr i# x)
{-# INLINE writeSmallArray #-}

lookup :: (Hashable k, Eq k) => HashTable s k v -> k -> ST s (Maybe v)
lookup table k = do
    let hashed = hash k
    (size :: Int) <- readByteArray (_size table) 0
    probe hashed hashed k size table
  where
    probe initHash hash k size table =
        readByteArray (_keyStates table) hash >>= \case
            (0 :: Word8) -> return Nothing
            _            -> readSmallArray (_keys table) hash >>= \case
                key | k == key -> Just <$> readSmallArray (_values table) hash
                _              -> if nextHash == initHash
                    then return Nothing
                    else probe initHash nextHash k size table
      where
        nextHash = (hash + 1) `rem` size

insert :: (Hashable k) => HashTable s k v -> k -> v -> ST s ()
insert = undefined

remove :: (Hashable k) => HashTable s k v -> k -> ST s ()
remove = undefined
